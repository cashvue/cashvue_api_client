from cashvue.api_client import APIClient


def get_all():
    """Calls the url /payee"""
    return APIClient().get('payee')


#
# You do not need any API to create a new payee. Just add the name to create entry and the new payee will be created
#
