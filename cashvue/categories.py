from cashvue.api_client import APIClient


def get_all():
    """Calls the url /category"""
    return APIClient().get('category')


def create(name, parent_category_id, description=None, color_code=None):
    return APIClient().post('category', body={"color_code": color_code,
                                              "name": name,
                                              "description": description,
                                              "parent_category_id": parent_category_id})


def delete(id):
    return APIClient().delete('category/{0}'.format(id))


def update(id, name=None, description=None, status=None, color_code=None):
    """Status takes values of INACTIVE or ACTIVE"""
    return APIClient().put('category/{0}'.format(id), {"name": name,
                                                       "description": description,
                                                       "status": status,
                                                       "color_code": color_code
                                                       })


def move(id, parent_category_id):
    """Use parent_category_id = 0 if you want to move the category to root"""
    return APIClient().put('category/{0}'.format(id), {"parent_category_id": parent_category_id})
