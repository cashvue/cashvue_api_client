from cashvue.api_client import APIClient


def get():
    """Calls the url /beginningbalance"""
    return APIClient().get('beginningbalance')
