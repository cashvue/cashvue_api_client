from cashvue.api_client import APIClient


def get(from_date=None, timely=None):
    """Calls the url /runway"""
    """timely takes values of daily, weekly, monthly"""
    return APIClient().get('runway', {'from': from_date, 'timely': timely})
