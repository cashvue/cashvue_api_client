from cashvue.api_client import APIClient


def get_all():
    """Calls the url /stream"""
    return APIClient().get('stream')
