from cashvue.api_client import APIClient


def get_repeat_entries(params=None):
    """Calls the url /repeatentry"""
    return APIClient().get('repeatentry', params=params)


def get_entries_in_repeat_entry(id, params=None):
    """Calls the url /repeatentry/id/entries"""
    return APIClient().get(f'repeatentry/{id}/entries', params=params)
