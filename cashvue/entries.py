from cashvue.api_client import APIClient
from enum import Enum


class EntryType(Enum):
    CASH_IN = 1
    CASH_OUT = 2


class PaymentTerms(Enum):
    ON_RECEIPT = 1
    NET_15 = 2
    NET_30 = 3
    NET_60 = 4
    NET_90 = 5


class EntryStatus(Enum):
    OPEN = "OPEN"
    PROJECTED = "PROJECTED"
    PAID = "PAID"
    CLEARED = "CLEARED"


def get_entries(params=None):
    """Calls the url /entry"""
    return APIClient().get('entry', params=params)


def get_filtered_entries(params=None, body=None):
    """
    {
    "sequence":["payment_date"],
    "payment_date":[["07/01/2019","01/01/2020"]],
    "entry_type":[{"id":1,"name":"CASH-IN"},{"id":2,"name":"CASH-OUT"}],
    "status":["PROJECTED","PAID","CLEARED"]
    }
    :param params:
    :param body:
    :return:
    """
    return APIClient().post('entry/filter', params=params, body=body)


def create(entry):
    """
    Sample create entry request. Use EntryStatus, EntryType and PaymentTerms
    {
      "status": "PROJECTED",
      "entry_type_id": "1",
      "amount": 10000,
      "category_id": 6,
      "stream_id": 1,
      "payment_date": "09/30/2019",
      "invoice_date": "09/06/2019",
      "due_date": "09/21/2019",
      "payment_terms_id": "2",
       "payee": "some payee",
       "tags": [{"id": 1, "name": "tag repellendus 322"}, {"name": "newtag"}],
       "memo": "some thing to remember",
       "ref_info": "ref"
     }
     """
    return APIClient().post('entry', body=entry)


def update(id, entry):
    return APIClient().put('entry/{0}'.format(id), body=entry)


def delete(id):
    return APIClient().delete('entry/{0}'.format(id))
