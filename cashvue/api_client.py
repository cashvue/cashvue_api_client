import requests
from cashvue import exceptions
from requests.exceptions import HTTPError


class APIClient:
    """Instantiate this APIClient to interact with cashvue REST APIs
       Call .configure to set the correct username and password for your cashvue account
    """
    __shared_state = {}
    cashvue_client_url_name = None
    cashvue_username = None
    cashvue_password = None
    cashvue_current_token = ''
    ssl_verify = True
    domain = 'cashvue.com'  # 'cashvue.local'

    def __init__(self):
        self.__dict__ = self.__shared_state

    def configure(self, cashvue_client_url_name, username, password):
        self.cashvue_client_url_name = f'https://{cashvue_client_url_name}.{self.domain}/api/v1.0'
        self.cashvue_username = username
        self.cashvue_password = password

    def validate(self):
        if not self.cashvue_client_url_name or not self.cashvue_username or not self.cashvue_password:
            raise exceptions.APIException("Url, Username and Password are not set")

    def login(self):
        self.validate()
        login_url = '{0}/login'.format(self.cashvue_client_url_name)
        login_data = {
          'email': self.cashvue_username,
          'password': self.cashvue_password
        }
        response = requests.post(login_url, json=login_data, verify=self.ssl_verify)
        if response.status_code == 200:
            json_response = response.json()
            self.cashvue_current_token = '' if json_response["errors"] else json_response["token"]
            return json_response
        else:
            raise exceptions.APIHTTPException(f'HTTP error occurred: {response.json()}')

    def get(self, resource, params=None):
        self.validate()
        get_url = '{0}/{1}'.format(self.cashvue_client_url_name, resource)
        req_headers = {'Content-Type': 'application/json', 'Authorization': self.cashvue_current_token}
        response = requests.get(get_url, params=params, headers=req_headers, verify=self.ssl_verify)
        try:
            response.raise_for_status()
        except HTTPError as http_err:
            raise exceptions.APIHTTPException(f'HTTP error occurred: {http_err}, {response.json()}')
        except Exception as err:
            raise exceptions.APIException(f'Other error occurred: {err}, {response.json()}')
        else:
            self.update_current_token(response)
            return response.json()

    def post(self, resource, params=None, body=None):
        self.validate()
        post_url = '{0}/{1}'.format(self.cashvue_client_url_name, resource)
        req_headers = {'Content-Type': 'application/json', 'Authorization': self.cashvue_current_token}
        response = requests.post(post_url, params=params, json=body, headers=req_headers, verify=self.ssl_verify)
        try:
            response.raise_for_status()
        except HTTPError as http_err:
            raise exceptions.APIHTTPException(f'HTTP error occurred: {http_err}, {response.json()}')
        except Exception as err:
            raise exceptions.APIException(f'Other error occurred: {err}, {response.json()}')
        else:
            self.update_current_token(response)
            return response.json()

    def put(self, resource, params=None, body=None):
        self.validate()
        put_url = '{0}/{1}'.format(self.cashvue_client_url_name, resource)
        req_headers = {'Content-Type': 'application/json', 'Authorization': self.cashvue_current_token}
        response = requests.put(put_url, params=params, json=body, headers=req_headers, verify=self.ssl_verify)
        try:
            response.raise_for_status()
        except HTTPError as http_err:
            raise exceptions.APIHTTPException(f'HTTP error occurred: {http_err}, {response.json()}')
        except Exception as err:
            raise exceptions.APIException(f'Other error occurred: {err}, {response.json()}')
        else:
            self.update_current_token(response)
            return response.json()

    def delete(self, resource, params=None, body=None):
        self.validate()
        delete_url = '{0}/{1}'.format(self.cashvue_client_url_name, resource)
        req_headers = {'Content-Type': 'application/json', 'Authorization': self.cashvue_current_token}
        response = requests.delete(delete_url, params=params, json=body, headers=req_headers, verify=self.ssl_verify)
        try:
            response.raise_for_status()
        except HTTPError as http_err:
            raise exceptions.APIHTTPException(f'HTTP error occurred: {http_err}, {response.json()}')
        except Exception as err:
            raise exceptions.APIException(f'Other error occurred: {err}, {response.json()}')
        else:
            self.update_current_token(response)
            return response.json()

    def update_current_token(self, response):
        if response.headers and response.headers['Authorization']:
            self.cashvue_current_token = response.headers['Authorization']
