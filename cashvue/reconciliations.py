from cashvue.api_client import APIClient


def get_all():
    """Calls the url /reconcile_entry"""
    return APIClient().get('reconcile_entry')
