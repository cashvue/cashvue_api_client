from cashvue.api_client import APIClient


def get_all():
    """Calls the url /tag"""
    return APIClient().get('tag')


#
# You do not need any API to create a new tag. Just add the name to create entry and the new tag will be created
#
