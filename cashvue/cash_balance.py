from cashvue.api_client import APIClient


def get(from_date=None, to_date=None):
    """Calls the url /cashbalance"""
    return APIClient().get('cashbalance', {"from": from_date, "to": to_date})
