import sys
from os.path import dirname
sys.path.append(dirname(__file__) + '/..')

import datetime
import json
from cashvue.api_client import APIClient
import cashvue.categories as categories
import cashvue.entries as entries
import cashvue.payees as payees
import cashvue.beginning_balance as beginning_balance
import cashvue.runway as runway
import cashvue.streams as streams
import cashvue.tags as tags
import cashvue.reconciliations as reconciliations
import cashvue.cash_balance as cash_balance
import cashvue.repeat_entries as repeat_entries

print("Cashvue API Cookbook")

#
# APIClient needs to be configured with cashvue url, username and password before calling any APIs
#
new_client = APIClient()
new_client.configure(cashvue_client_url_name='demo',
                     username='demo@cashvue.com',
                     password='demo1234')


#
# Call login() API first to continue calling other APIs. login api needs to be called only one time for every session.
#
new_client.login()

#
# Get Categories
#
# all_categories = categories.get_all()
# # print(all_categories)
#
# #
# # Get Payees
# #
# all_payees = payees.get_all()
# print(all_payees)
#
# #
# # Get Tags
# #
# all_tags = tags.get_all()
# print(all_tags)
#
# #
# # Get Streams
# #
# all_streams = streams.get_all()
# print(all_streams)
#
# #
# # Get reconciliations
# #
# all_reconciliations = reconciliations.get_all()
# print(all_reconciliations)

#
# Get runway
# timely takes values of daily, weekly and monthly
#
# runway_data = runway.get(timely="/")
# print(runway_data)

#
# Get beginning_balance
#
# beginning_balance_data = beginning_balance.get()
# print(beginning_balance_data)

#
# Get cashbalance
#
# today = datetime.date.today()
# cashbalance_data = cash_balance.get(from_date=today)
# print(cashbalance_data)

#
# Get repeat_entries/projections
#
# repeat_entries_data = repeat_entries.get_repeat_entries({"page": 1})
# print(repeat_entries_data)

#
# Get entries in a repeat_entries/projection
#
# entries_in_repeat_entry_1 = repeat_entries.get_entries_in_repeat_entry(4)
# print(entries_in_repeat_entry_1)

#
# Create new Category
#
# new_category = categories.create(name="Credit",
#                                  parent_category_id=36,
#                                  description="api testing",
#                                  color_code="#f9cb9c")
# print(new_category)

#
# Delete a category
#
# deleted_category = categories.delete(new_category['id'])
# print(deleted_category)

#
# Move a Category
#
# moved_category = categories.move(5, 0)  # Use parent_category_id = 0 if you want to make this the root category
# print(moved_category)
# moved_category_2 = categories.move(5, 6)
# print(moved_category_2)


#
# Create a new Entry
#
# invoice_date = datetime.datetime.now().strftime("%m/%d/%Y")
# due_date = (datetime.datetime.now() + datetime.timedelta(days=15)).strftime("%m/%d/%Y")
# payment_date = (datetime.datetime.now() + datetime.timedelta(days=10)).strftime("%m/%d/%Y")
#
# new_entry = entries.create({
#       "status": entries.EntryStatus.PROJECTED.value,
#       "entry_type_id": entries.EntryType.CASH_IN.value,
#       "amount": 12000,  # amount is always in cents
#       "category_id": 6,
#       "stream_id": 1,
#       "payment_date": payment_date,
#       "invoice_date": invoice_date,
#       "due_date": due_date,
#       "payment_terms_id": entries.PaymentTerms.NET_30.value,
#       "payee": "some payee",
#       "tags": [{"id": 1, "name": "tag repellendus 322"}, {"name": "newtag"}],
#       "memo": "some thing to remember",
#       "ref_info": "reference information"
#      })
# print(new_entry)

#
# Update an Entry
#
# updated_entry = entries.update(new_entry["entry"]["id"], {"status": entries.EntryStatus.CLEARED.value})
#
# print(updated_entry)

#
# Delete an Entry
#
# deleted_entry = entries.delete(new_entry["entry"]["id"])
#
# print(deleted_entry)


#
# Get entries with Filters
#
today = datetime.datetime.now().strftime("%m/%d/%Y")
month_from_now = (datetime.datetime.now() + datetime.timedelta(days=30)).strftime("%m/%d/%Y")

few_entries = entries.get_filtered_entries(params={"limit": 50,
                                                   "page": 1,
                                                   "sort": "payment_date",
                                                   "sortdir": "desc"},
                                           body={
                                                 "payment_date": [[today, month_from_now]],
                                                 "entry_type": [{"id": entries.EntryType.CASH_OUT.value}],
                                                 "status": [entries.EntryStatus.PROJECTED.value,
                                                            entries.EntryStatus.PAID.value]
                                                 })

print(few_entries)
