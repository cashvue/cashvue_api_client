import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cashvue_api_client",
    version="0.0.1",
    author="Anil",
    author_email="yak@fastmail.com",
    description="A python package to integrate with Cashvue",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cashvue/cashvue_api_client",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
