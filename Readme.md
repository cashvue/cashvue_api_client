# Cashvue API client

Sample Python client that calls Cashvue REST APIs. This code helps you integrate any third party data systems with cashvue.

## How to install?

### Install Directly from Gitlab

```bash
pip install git+https://gitlab.com/cashvue/cashvue_api_client
```

now you can import from cashvue module using 

```python
from cashvue.api_client import APIClient
import cashvue.categories as categories
import cashvue.entries as entries
```

### Install from Source
This project is written and tested on python 3.6 or later versions. we use pipenv to manage dependencies so install pipenv using

```bash
pip install pipenv
```

and then run pipenv install to install all project dependencies

```bash
pipenv install
```

## How to use the library?

Refer to `samples/cookbook.py` for how to use the API

```pipenv run python samples/cookbook.py```


## General Overview

These are the following resources available in Cashvue

- entry, 
- category, 
- payee, 
- tag, 
- stream, 
- runway, 
- beginningbalance, 
- repeatentry 

>Use get api in api_client.py to get all the resources

>Use put api to update an existing resource

>Use post api to create a new resource

>Use delete api to delete a particular resource

Configure the credentials of the client account before calling the APIs which need Authorization.

```python
from cashvue.api_client import APIClient
new_client = APIClient()
new_client.configure(cashvue_client_url_name='demo',
                     username='demo@cashvue.com',
                     password='demo1234')
```

If your account is `demo` the cashvue_client_url_name is `demo`, this helps the code construct the domain url for your account at https://demo.cashvue.com

## Authentication
Call the login() method in api_client to authenticate your credentials with Cashvue server. Once your login api call to cashvue succeeds, the client takes care of handling the authentication token for you in future calls. This token expires after 1 hour of inactivity from client code. Call login again if your token expires.

```python
from cashvue.api_client import APIClient
new_client = APIClient()
new_client.configure(cashvue_client_url_name='demo',
                     username='demo@cashvue.com',
                     password='demo1234')
new_client.login()
```
